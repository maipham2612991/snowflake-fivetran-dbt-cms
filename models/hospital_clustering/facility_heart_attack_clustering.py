import snowflake.snowpark.functions as F

import logging

import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA


logger = logging.getLogger("facility_heart_attack_clustering")


def model(dbt, session):
    # Must be either table or incremental (view is not currently supported)
    dbt.config(
        packages = ['scikit-learn', 'numpy', 'pandas'],
        materialized = "table"
    )
    condition = "Heart Attack or Chest Pain"

    # select facility + measures in condition
    measure_df = session.table("measure_dimension") \
                        .filter(F.col("condition") == condition) \
                        .select("measure_id")
    measure_ids = [row['measure_id'] for row in measure_df.collect(case_sensitive=False)]

    df = session.table("facility_measure_score_pivot") \
                .select(['facility_id'] + measure_ids)

    # fillna for ML
    df = df.dropna(subset=measure_ids, how='all') \
            .fillna(-150, subset=['OP_2']) \
            .fillna(500, subset=['OP_3B']) \
            .withColumn('cluster', F.when(F.col('OP_3B') <= 100, 1)
                                    .when(F.col('OP_2') > 60, 2)
                                    .otherwise(0))

    return df
import snowflake.snowpark.functions as F

import logging

import pandas as pd
from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.cluster import KMeans

from math import sqrt


logger = logging.getLogger("facility_sepsis_care_clustering")


def _pca(df, features):
    pca = PCA(n_components=2)  # Adjust the number of components as needed
    principal_components = pca.fit_transform(df[features])

    # Creating a DataFrame with the principal components
    pca_df = pd.DataFrame(data=principal_components, columns=['PC1', 'PC2'])

    df['PC1'] = pca_df['PC1']
    df['PC2'] = pca_df['PC2']

    return df


def _linear_model(df, features, target):
    df_train = df[~df[target].isnull()]
    df_test = df[df[target].isnull()]

    X = df_train[features]
    y = df_train[target]

    model = LinearRegression()
    model.fit(X, y)

    y_pred = model.predict(df_test[features])
    df.loc[df[target].isnull(), target] = y_pred

    logger.info("Done training linear model")
    y_train_pred = model.predict(X)
    # Calculate metrics
    mae = mean_absolute_error(y, y_train_pred)
    mse = mean_squared_error(y, y_train_pred)
    rmse = sqrt(mse)
    r2 = r2_score(y, y_train_pred)

    logger.info(f"Mean Absolute Error: {mae}")
    logger.info(f"Mean Squared Error: {mse}")
    logger.info(f"Root Mean Squared Error: {rmse}")
    logger.info(f"R-squared: {r2}")

    return df


def _kmeans(df, n_clusters, features):
    kmeans = KMeans(n_clusters=n_clusters, random_state=40)
    clusters = kmeans.fit_predict(df[features])
    df['cluster'] = clusters

    return df


def model(dbt, session):
    # Must be either table or incremental (view is not currently supported)
    dbt.config(
        packages = ['scikit-learn', 'numpy', 'pandas'],
        materialized = "table"
    )
    condition = "Sepsis Care"

    # select facility + measures in condition
    measure_df = session.table("measure_dimension") \
                        .filter(F.col("condition") == condition) \
                        .select("measure_id")
    measure_ids = [row['measure_id'] for row in measure_df.collect(case_sensitive=False)]

    df = session.table("facility_measure_score_pivot") \
                .select(['facility_id'] + measure_ids)

    # fillna by linear regression
    logger.info("Starting fillna by linear regression")
    linear_features = ['SEP_SH_3HR', 'SEV_SEP_3HR', 'SEP_1', 'SEV_SEP_6HR']
    df = df.dropna(subset=linear_features, how='any') 
    pandas_df = _linear_model(
        df=df.to_pandas(),
        features=linear_features,
        target='SEP_SH_6HR'
    )

    # PCA
    logger.info("Starting PCA...")
    pandas_df = _pca(pandas_df, features=measure_ids)

    # Kmeans
    logger.info("Starting clustering...")
    pandas_df = _kmeans(pandas_df, n_clusters=3, features=measure_ids)

    df = session.write_pandas(
        pandas_df, 
        table_name="pca_sepsis_care", 
        auto_create_table=True, 
        table_type='temp'
    )

    return df
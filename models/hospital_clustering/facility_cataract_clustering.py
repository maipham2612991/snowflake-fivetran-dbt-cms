import snowflake.snowpark.functions as F
import logging

logger = logging.getLogger('facility_cataract_clustering')

def model(dbt, session):
    # Must be either table or incremental (view is not currently supported)
    dbt.config(materialized = "table")
    condition = "Cataract surgery outcome"
    unique_column = 'OP_31'

    # select facility + measures in condition
    df = session.table("facility_measure_score_pivot") \
                .select(['facility_id', unique_column])

    # Filter Na
    df = df.filter(F.col(unique_column).isNotNull())

    logger.info("Clustering by threshold...")
    # cluster by threshold
    df = df.withColumn('cluster', F.when(F.col(unique_column) >= 90, '>=90%')
                                    .otherwise('<90%'))
    return df
import snowflake.snowpark.functions as F

import logging

import pandas as pd
from sklearn.cluster import KMeans


logger = logging.getLogger("facility_vaccination_clustering")


def _kmeans(df, num_clusters):
    kmeans = KMeans(n_clusters=num_clusters, random_state=40)
    clusters = kmeans.fit_predict(df.iloc[:, 1:])
    df['cluster'] = clusters

    return df


def model(dbt, session):
    dbt.config(
        packages = ['scikit-learn', 'numpy', 'pandas'],
        materialized = "table"
    )
    condition = "Healthcare Personnel Vaccination"

    # This condition has two measures: HCP_COVID_19, IMM_3
    df = session.table("facility_measure_score_pivot") \
                .select(['facility_id', 'HCP_COVID_19', 'IMM_3'])

    # filter na
    df = df.dropna(subset=['HCP_COVID_19', 'IMM_3'], how='any')

    df = session.write_pandas(
        _kmeans(df=df.to_pandas(), num_clusters=4), 
        table_name="pca_vaccination", 
        auto_create_table=True, 
        table_type='temp'
    )

    return df
import snowflake.snowpark.functions as F

import logging

import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from functools import reduce
from sklearn.cluster import KMeans


logger = logging.getLogger("facility_electronic_clinic_clustering")


def _pca(df, components):
    scaler = MinMaxScaler()
    scaled_data = scaler.fit_transform(df.iloc[:, 1:])

    pca = PCA(n_components=len(components))  # Adjust the number of components as needed
    principal_components = pca.fit_transform(scaled_data)

    # Creating a DataFrame with the principal components
    pca_df = pd.DataFrame(data=principal_components, columns=components)

    for component in components:
        df[component] = pca_df[component]

    return df


def _kmeans(df, n_clusters, features):
    kmeans = KMeans(n_clusters=n_clusters, random_state=40)
    clusters = kmeans.fit_predict(df[features])
    df['cluster'] = clusters

    return df


def model(dbt, session):
    # Must be either table or incremental (view is not currently supported)
    dbt.config(
        packages = ['scikit-learn', 'numpy', 'pandas'],
        materialized = "table"
    )
    condition = "Electronic Clinical Quality Measure"

    # select facility + measures in condition
    measure_df = session.table("measure_dimension") \
                        .filter(F.col("condition") == condition) \
                        .select("measure_id")
    measure_ids = [row['measure_id'] for row in measure_df.collect(case_sensitive=False)]
    df = session.table("facility_measure_score_pivot") \
                .select(['FACILITY_ID'] + measure_ids)

    # fillna for ML
    df = df.dropna(subset=measure_ids, how='all')

    # PCA for measure: the lower the better
    df_lower = df[['FACILITY_ID', 'ED_2_STRATA_1', 'ED_2_STRATA_2', 'SAFE_USE_OF_OPIOIDS']] \
                .dropna(subset=['ED_2_STRATA_2', 'SAFE_USE_OF_OPIOIDS'], how='all') \
                .filter(~(F.col('ED_2_STRATA_2') > df.approxQuantile('ED_2_STRATA_2', [0.99])[0])) \
                .filter(~(F.col('ED_2_STRATA_1') > df.approxQuantile('ED_2_STRATA_1', [0.99])[0])) \
                .fillna(600, subset=['ED_2_STRATA_1']) \
                .fillna(1300, subset=['ED_2_STRATA_2']) \
                .fillna(150, subset=['SAFE_USE_OF_OPIOIDS'])
    pandas_df_lower = _pca(df_lower.to_pandas(), components=['PC1'])

    # PCA for measure: the higher percentage the better
    df_higher = df[['FACILITY_ID', 'STK_02', 'STK_05', 'STK_06', 'VTE_1', 'VTE_2']] \
                .dropna(subset=['VTE_1', 'VTE_2'], how='all') \
                .fillna(-50)
    pandas_df_higher = _pca(df_higher.to_pandas(), components=['PC2'])
    
    # join two df and kmeans
    df_merge = pandas_df_lower.merge(pandas_df_higher, on='FACILITY_ID', how='inner')
    df_merge = _kmeans(
        df_merge, 
        n_clusters=4, 
        features=['ED_2_STRATA_1', 'ED_2_STRATA_2', 'SAFE_USE_OF_OPIOIDS', 'STK_02', 'STK_05', 'STK_06', 'VTE_1', 'VTE_2']
    )


    df = session.write_pandas(
        df_merge, 
        table_name="pca_eletronic_clinic", 
        auto_create_table=True, 
        table_type='temp'
    )

    return df
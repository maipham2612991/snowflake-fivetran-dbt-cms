import snowflake.snowpark.functions as F

import logging

import pandas as pd
from sklearn.preprocessing import StandardScaler, OrdinalEncoder
from sklearn.decomposition import PCA
from functools import reduce
from sklearn.cluster import KMeans


logger = logging.getLogger("facility_emergency_clustering")


def _pca(df, scaled_data, components):
    pca = PCA(n_components=len(components))  # Adjust the number of components as needed
    principal_components = pca.fit_transform(scaled_data)

    # Creating a DataFrame with the principal components
    pca_df = pd.DataFrame(data=principal_components, columns=components)

    for component in components:
        df[component] = pca_df[component]

    return df


def _kmeans(df, n_clusters, scaled_data):
    kmeans = KMeans(n_clusters=n_clusters, random_state=40)
    clusters = kmeans.fit_predict(scaled_data)
    df['cluster'] = clusters

    return df

def _preprocess_EDV(df: pd.DataFrame):
    categories = ['very high', 'high', 'medium', 'low' ]
    encoder = OrdinalEncoder(categories=[categories])
    df['EDV_encoded'] = encoder.fit_transform(df[['EDV']])

    return df


def model(dbt, session):
    # Must be either table or incremental (view is not currently supported)
    dbt.config(
        packages = ['scikit-learn', 'numpy', 'pandas'],
        materialized = "table"
    )
    condition = "Emergency Department"

    # select facility + measures in condition
    measure_df = session.table("measure_dimension") \
                        .filter(F.col("condition") == condition) \
                        .select("measure_id")
    measure_ids = [row['measure_id'] for row in measure_df.collect(case_sensitive=False)]
    df = session.table("facility_measure_score_pivot") \
                .select(['FACILITY_ID'] + measure_ids)

    # dropna and fill 
    df = df.dropna(subset=['OP_18B', 'EDV', 'OP_18C', 'OP_22'], how='any') \
            .fillna(-150, subset=['OP_23']) \
            .filter(F.col('OP_18C') <= df.approxQuantile('OP_18C', [0.99])[0])

    pandas_df = df.to_pandas()
    pandas_df = _preprocess_EDV(pandas_df)

    features = ['OP_18B', 'EDV_encoded', 'OP_18C', 'OP_22', 'OP_23']
    scaler = StandardScaler()
    scaled_data = scaler.fit_transform(pandas_df[features])
    pandas_df = _pca(df=pandas_df, scaled_data=scaled_data, components=['PC1', 'PC2'])
    pandas_df = _kmeans(df=pandas_df, n_clusters=3, scaled_data=scaled_data)

    df = session.write_pandas(
        pandas_df[['FACILITY_ID', 'OP_18B', 'EDV_encoded', 'OP_18C', 'OP_22', 'OP_23', 'PC1', 'PC2', 'cluster']], 
        table_name="pca_emergency", 
        auto_create_table=True, 
        table_type='temp'
    )

    return df
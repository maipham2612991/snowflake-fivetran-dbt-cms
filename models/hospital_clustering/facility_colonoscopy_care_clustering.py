import snowflake.snowpark.functions as F

def model(dbt, session):
    dbt.config(materialized = "table")
    condition = "Colonoscopy care"
    unique_column = 'OP_29'

    # select facility + measures in condition
    df = session.table("facility_measure_score_pivot") \
                .select(['facility_id', unique_column])

    # Filter Na
    df = df.filter(F.col(unique_column).isNotNull())

    # cluster by threshold
    df = df.withColumn('cluster', F.when((0 <= F.col(unique_column)) & (F.col(unique_column) <= 40), '0->40%')
                                    .when((40 <= F.col(unique_column)) & (F.col(unique_column) <= 70), '40->70%')
                                    .when((70 <= F.col(unique_column)) & (F.col(unique_column) <= 90), '70->90%')
                                    .otherwise('>90%'))
    return df
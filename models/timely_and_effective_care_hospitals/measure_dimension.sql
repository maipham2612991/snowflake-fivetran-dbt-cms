
-- Use the `ref` function to select from other models

{{ config(materialized='table') }}

WITH MEASURE AS (
    SELECT
        DISTINCT
            MEASURE_ID,
            MEASURE_NAME,
            CONDITION
    FROM {{ source("s3", "timely_and_effective_care_hospital") }}
)

SELECT
    *
FROM MEASURE

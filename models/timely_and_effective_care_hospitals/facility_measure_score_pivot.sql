
{{ config(materialized="table") }}


WITH FACILITY_MEASURE_SCORE_PIVOT AS (
    SELECT
        *
    FROM (
        SELECT
            FACILITY_ID,
            MEASURE_ID,
            SCORE
        FROM {{ ref('facility_measure_score_fact') }}
    )
    PIVOT (
        MAX(SCORE) FOR MEASURE_ID IN ('EDV', 'ED_2_Strata_1', 'ED_2_Strata_2', 'HCP_COVID_19', 'IMM_3', 'OP_18c', 'OP_2', 'OP_22', 'OP_23', 'OP_31', 'OP_3b', 'SEP_SH_3HR', 'STK_02', 'STK_03', 'STK_05', 'OP_29', 'SEP_SH_6HR', 'SEV_SEP_3HR', 'VTE_1', 'VTE_2', 'OP_18b', 'SAFE_USE_OF_OPIOIDS', 'SEP_1', 'SEV_SEP_6HR', 'STK_06')
    )
)

SELECT
    FACILITY_ID,
    "'EDV'" AS EDV,
    CAST("'ED_2_Strata_1'" AS INTEGER) AS ED_2_Strata_1,
    CAST("'ED_2_Strata_2'" AS INTEGER) AS ED_2_Strata_2,
    CAST("'HCP_COVID_19'" AS FLOAT) AS HCP_COVID_19,
    CAST("'IMM_3'" AS INTEGER) AS IMM_3,
    CAST("'OP_18c'" AS INTEGER) AS OP_18c,
    CAST("'OP_2'" AS INTEGER) AS OP_2,
    CAST("'OP_22'" AS INTEGER) AS OP_22,
    CAST("'OP_23'" AS INTEGER) AS OP_23,
    CAST("'OP_31'" AS INTEGER) AS OP_31,
    CAST("'OP_3b'" AS INTEGER) AS OP_3b,
    CAST("'SEP_SH_3HR'" AS INTEGER) AS SEP_SH_3HR,
    CAST("'STK_02'" AS INTEGER) AS STK_02,
    CAST("'STK_03'" AS INTEGER) AS STK_03,
    CAST("'STK_05'" AS INTEGER) AS STK_05,
    CAST("'OP_29'" AS INTEGER) AS OP_29,
    CAST("'SEP_SH_6HR'" AS INTEGER) AS SEP_SH_6HR,
    CAST("'SEV_SEP_3HR'" AS INTEGER) AS SEV_SEP_3HR,
    CAST("'VTE_1'" AS INTEGER) AS VTE_1,
    CAST("'VTE_2'" AS INTEGER) AS VTE_2,
    CAST("'OP_18b'" AS INTEGER) AS OP_18b,
    CAST("'SAFE_USE_OF_OPIOIDS'" AS INTEGER) AS SAFE_USE_OF_OPIOIDS,
    CAST("'SEP_1'" AS INTEGER) AS SEP_1,
    CAST("'SEV_SEP_6HR'" AS INTEGER) AS SEV_SEP_6HR,
    CAST("'STK_06'" AS INTEGER) AS STK_06
FROM FACILITY_MEASURE_SCORE_PIVOT
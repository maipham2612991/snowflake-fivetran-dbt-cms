{{ config(materialized='table') }}

WITH FACILITY AS (
    SELECT
        DISTINCT
            FACILITY_ID,
            FACILITY_NAME,
            ADDRESS,
            CITY_TOWN,
            STATE,
            CAST(ZIP_CODE AS INT) AS ZIP_CODE,
            COUNTY_PARISH,
            TELEPHONE_NUMBER
    FROM {{ source("s3", "timely_and_effective_care_hospital") }}
)

select *
from FACILITY

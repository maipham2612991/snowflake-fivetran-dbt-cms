
{{ config(materialized="table") }}


WITH FACILITY_MEASURE_SCORE AS (
    SELECT
        DISTINCT
            FACILITY_ID,
            MEASURE_ID,
            SCORE,
            "SAMPLE",
            FOOTNOTE
    FROM {{ source("s3", "timely_and_effective_care_hospital") }}
)

SELECT
    FACILITY_ID,
    MEASURE_ID,
    CASE
        WHEN SCORE = 'Not Available' THEN NULL
        ELSE SCORE
    END AS SCORE,
    CASE
        WHEN "SAMPLE" = 'Not Available' THEN NULL
        ELSE CAST("SAMPLE" AS INTEGER)
    END AS "SAMPLE",
    CASE
        WHEN FOOTNOTE LIKE '%,%' THEN CAST(SPLIT(FOOTNOTE, ', ')[1] AS INTEGER)
        ELSE CAST(FOOTNOTE AS INTEGER)
    END AS FOOTNOTE
FROM FACILITY_MEASURE_SCORE
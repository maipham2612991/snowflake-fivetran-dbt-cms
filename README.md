# snowflake-fivetran-dbt-cms

## Getting started

This project aimed to divide hospitals into clusters. By knowing more about the characteristic of each hospital cluster, we can know which hospitals are good at which medical measures.

## Architecture

![alt text](./hospital-clustering-architecture.png)

## Ouput

[hospital-clustering](https://app.snowflake.com/izebnuy/mrb27662/#/streamlit-apps/PC_DBT_DB.DBT_MPHAM.U84QLDJUJF2KJK9F)

Login:
```
username: result_viewer
pass: Resultviewer1
```

The link will expire as the snowflake free trial ended.

This is the result demos:

![alt text](./output_streamlit.png)

The source code for streamlit app can be seen at  <br>
https://gitlab.com/maipham2612991/streamlit-cms-clustering.git 

## Drawbacks
- dbt does not allow reuse code in Python models
- Snowflake streamlit has not have some features: modify streamlit version config, not integrate git
- Snowflake components: not supports cross account features

{% test datatype_check(model, column_name, expected_datatype) %}

select
    *
from
    "{{ target.database }}".INFORMATION_SCHEMA.COLUMNS
where
    table_catalog = '{{ target.database }}'
    and table_schema = upper('{{ target.schema }}')
    and table_name = upper('{{ model }}')
    and column_name = upper('{{ column_name }}')
    and data_type != upper('{{ expected_datatype }}')

{% endtest %}

